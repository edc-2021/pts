import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from './pages/auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'PTSPOS';

  private authListenerSubscription: Subscription;
  isAuthenticated: Boolean = false;
  constructor(private authService: AuthService) {}


  

  onLogOut() {
    this.authService.logout();
  }
  ngOnInit() {
    //auto login
    this.authService.autoAuthUser();
    this.isAuthenticated = this.authService.getAuthenticatedStatus();
    this.authListenerSubscription = this.authService
      .getAuthStatusListener()
      .subscribe((isAuthenticated) => {
        this.isAuthenticated = isAuthenticated;
      });
  }
  ngOnDestroy() {
    this.authListenerSubscription.unsubscribe();
  }
}
