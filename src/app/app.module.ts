import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SideNavComponent } from './pages/common/sidenav/sidenav.component';
import { PrinterComponent } from './pages/printer/printer.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ReportComponent } from './pages/reports/reports.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { DynamicScriptLoaderService } from './pages/DynamicScriptLoaderService';
import { PumpComponent } from './pages/pump/pump.component';
import { HeaderComponent } from './pages/common/header/header.component';
import { FooterComponent } from './pages/common/footer/footer.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './pages/auth/login/login.component';
import { CommonModule } from '@angular/common';
import { AuthGuard } from './pages/auth/auth.guard';
import { AuthInterceptor } from './pages/auth/auth.interceptor';
import { SettingsComponent } from './pages/settings/settings.component';
import { ReceiptComponent } from './pages/receipts/receipts.component';

@NgModule({
  declarations: [
    AppComponent,
    SideNavComponent,
    PrinterComponent,
    DashboardComponent,
    ReportComponent,
    PumpComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    SettingsComponent,
    ReceiptComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
  ],
  providers: [
    DynamicScriptLoaderService,
    AuthGuard,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
