import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './pages/auth/auth.guard';
import { LoginComponent } from './pages/auth/login/login.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { DiscountComponent } from './pages/discount/discount.component';
import { PrinterComponent } from './pages/printer/printer.component';
import { PumpComponent } from './pages/pump/pump.component';
import { ReceiptComponent } from './pages/receipts/receipts.component';
import { ReportComponent } from './pages/reports/reports.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { TransactionsComponent } from './pages/transactions/transactions.component';

const routes: Routes = [
  { path: '', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'printers', component: PrinterComponent, canActivate: [AuthGuard] },
  { path: 'discounts', component: DiscountComponent, canActivate: [AuthGuard] },
  { path: 'receipts', component: ReceiptComponent, canActivate: [AuthGuard] },
  {
    path: 'transactions',
    component: TransactionsComponent,
    canActivate: [AuthGuard],
  },
  { path: 'reports', component: ReportComponent, canActivate: [AuthGuard] },
  { path: 'settings', component: SettingsComponent, canActivate: [AuthGuard] },
  { path: 'pumps', component: PumpComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
