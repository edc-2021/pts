
export interface Customer {
   id:string,
   name:string,
   phone:string,
   address:string,
   vrn:string,
   cardType:string,
   cardNumber:string 
}

export interface CustomerResponse {
    code:string,
    message:string
}

export interface CustomerDiscount {
    customerId:string,
    pumpId:string,
    nozzleId:string,
    discount:string
}