import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { Customer, CustomerDiscount, CustomerResponse } from './customer.model';

@Injectable({ providedIn: 'root' })
export class CustomerService {
  private customers: Customer[] = [];
  private discounts: CustomerDiscount[] = [];
  private updatedDiscounts = new Subject<CustomerDiscount[]>();
  private updatedCustomers = new Subject<Customer[]>();

  private API_URL: String = 'http://localhost:3000/api/v1/';
  constructor(private http: HttpClient, private router: Router) {}

  getCustomers() {
    return this.http
      .get<{ customer: Customer[] }>(this.API_URL + 'customers')
      .subscribe((customerData) => {
        this.customers = customerData.customer;
        this.updatedCustomers.next([...this.customers]);
      });
  }

  getSingleCustomer(customerId: String): Customer {
    const customer = this.customers.findIndex((c) => c.id == customerId);
    return this.customers[customer];
  }

  addCustomer(customer: Customer) {
    return this.http
      .post<{ addedCustomer: CustomerResponse }>(
        this.API_URL + 'customers',
        customer
      )
      .subscribe((customerData) => {
        console.log(customerData.addedCustomer.message);
        this.customers.push(customer);
        this.updatedCustomers.next([...this.customers]);
        this.router.navigate(['/discounts']);
      });
  }

  updateCustomer(customer: Customer) {
    return this.http
      .put<{ updatedCustomer: CustomerResponse }>(
        this.API_URL + 'customers' + customer.id,
        customer
      )
      .subscribe((customerData) => {
        console.log(customerData.updatedCustomer.message);
        this.router.navigate(['/discounts']);
      });
  }

  deleteCustomer(customerId: String) {
    return this.http
      .delete<{ deletedCustomer: CustomerResponse }>(
        this.API_URL + 'customers' + customerId
      )
      .subscribe((customerData) => {
        console.log(customerData.deletedCustomer.message);
        const updateCustomer = this.customers.filter(
          (customer) => customer.id != customerId
        );
        this.customers = updateCustomer;
        this.updatedCustomers.next([...this.customers]);
      });
  }

  giveCustomerDiscount(customerDiscount: CustomerDiscount) {
    return this.http
      .post<{ discountRes: CustomerResponse }>(
        this.API_URL + 'customers/discounts',
        customerDiscount
      )
      .subscribe((discountData) => {
        console.log(discountData.discountRes.message);
        this.discounts.push(customerDiscount);
        this.updatedDiscounts.next([...this.discounts]);
        this.router.navigate(['/discounts']);
      });
  }
}
