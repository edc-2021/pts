import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Printer } from './printer.model';
import { PrinterService } from './printer.service';

@Component({
  selector: 'app-printer-component',
  templateUrl: './printer.component.html',
})
export class PrinterComponent implements OnInit, OnDestroy {
  printers: Printer[] = [];
  private printerSubscription: Subscription = new Subscription();
  constructor(private printerService: PrinterService) {}
  ngOnInit() {
    this.printerService.getPrinters();
    this.printerSubscription = this.printerService
      .getPrinterUpdateListener()
      .subscribe((printers) => {
        this.printers = printers;
      });
  }
  ngOnDestroy() {
    this.printerSubscription.unsubscribe();
  }
}
