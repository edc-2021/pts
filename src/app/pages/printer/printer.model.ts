export interface Printer {
  id: string;
  name: string;
  ip: string;
}

export interface PrinterResponse {
  code: string;
  message: string;
}
