import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { Printer, PrinterResponse } from './printer.model';

@Injectable({ providedIn: 'root' })
export class PrinterService {
  private printers: Printer[] = [];
  private updatedPrinters = new Subject<Printer[]>();

  private API_URL = 'http://localhost:1044/api/v1/';
  constructor(private http: HttpClient, private router: Router) {}

  getPrinters() {
    return this.http
      .get<{}>(this.API_URL + 'printers')
      .subscribe((printerData) => {
        console.log('Getting printers');
        console.log(printerData);
        this.printers = printerData as Printer[];
        this.updatedPrinters.next([...this.printers]);
      });
  }

  getPrinterUpdateListener() {
    return this.updatedPrinters.asObservable();
  }

  addPrinter(printer: Printer) {
    return this.http
      .post<{ addedPrinter: PrinterResponse }>(
        this.API_URL + 'printer',
        printer
      )
      .subscribe((printerData) => {
        console.log(printerData.addedPrinter.message);
        this.printers.push(printer);
        this.updatedPrinters.next([...this.printers]);
        this.router.navigate(['/printers']);
      });
  }

  updatePrinter(printer: Printer) {
    return this.http
      .put<{ updated: PrinterResponse }>(
        this.API_URL + 'printer' + printer.id,
        printer
      )
      .subscribe((printerData) => {
        console.log(printerData.updated.message);
        this.router.navigate(['/printers']);
      });
  }

  deletePrinter(printerId: String) {
    return this.http
      .delete<{ deletedPrinter: PrinterResponse }>(
        this.API_URL + 'printer' + printerId
      )
      .subscribe((printerData) => {
        console.log(printerData.deletedPrinter.message);
        const updatePrinter = this.printers.filter(
          (printer) => printer.id != printerId
        );
        this.printers = updatePrinter;
        this.updatedPrinters.next([...this.printers]);
      });
  }
}
