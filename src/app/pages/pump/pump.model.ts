export interface Pump {
  id: string;
  port: string;
  //pump address
  address: string;
  status: string;
  //printer ip
  ip: string;
  // nozzle
}

export interface PumpUpdate {
  id: string;
  name: string;
  printerId: string;
}

export interface PumpUpdateResponse {
  code:string,
  message:string
}