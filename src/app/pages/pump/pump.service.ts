import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { Pump, PumpUpdate, PumpUpdateResponse } from './pump.model';

@Injectable({ providedIn: 'root' })
export class PumpService {
  private pumps: Pump[] = [];
  private updatedPumps = new Subject<Pump[]>();

  private API_URL: string = 'http://localhost:1044/api/v1/';
  constructor(private http: HttpClient, private router: Router) {}

  getPumps() {
    return this.http.get<{}>(this.API_URL + 'pumps').subscribe((pumpData) => {
      console.log('Getting pumps');
      console.log(pumpData);
      this.pumps = pumpData as Pump[];
      this.updatedPumps.next([...this.pumps]);
    });
  }

  getPumpUpdateListener() {
    return this.updatedPumps.asObservable();
  }

  updatePump(pump: PumpUpdate) {
    var header = {
      headers: new HttpHeaders().set(
        'apiKey',
        'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEsImlhdCI6MTYxNzc5MDg0MCwiZXhwIjoxNjE4Mzk1NjQwfQ.-m7yNGkFoIeoXe8HWI7INaX_PYHfsc2Upos_2DcZF0c'
      ),
    };

    return this.http
      .put<{ updated: PumpUpdateResponse }>(
        this.API_URL + 'pumps/' + pump.id,
        pump,
        header
      )
      .subscribe((pumpData) => {
        console.log(pumpData.updated.message);
        this.router.navigate(['/pumps']);
      });
  }
}
