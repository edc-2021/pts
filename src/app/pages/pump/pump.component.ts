import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Pump } from './pump.model';
import { PumpService } from './pump.service';

@Component({
  selector: 'app-pump-component',
  templateUrl: './pump.component.html',
})
export class PumpComponent implements OnInit, OnDestroy {
  pumps: Pump[] = [];
  private pumpSubscription: Subscription = new Subscription();
  constructor(private pumpService: PumpService) {}
  ngOnInit() {
    this.pumpService.getPumps();
    this.pumpSubscription = this.pumpService
      .getPumpUpdateListener()
      .subscribe((pumps: Pump[]) => {
        this.pumps = pumps;
      });
  }

  ngOnDestroy() {
    this.pumpSubscription.unsubscribe();
  }
}
