import { Component } from '@angular/core';

declare var $: any;
@Component({
  selector: 'app-report-component',
  templateUrl: './reports.component.html',
})
export class ReportComponent {
  ngOnInit() {
    this.loadScripts();
  }

  private loadScripts() {
    // this.loadScripts();
    $('input[name="daterange"]').daterangepicker({
      opens: 'left',
      locale: {
        format: 'DD/MM/YYYY',
      },
    });

    $('#datetimepicker-minimum').datetimepicker();
  }
}
