import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { OilProduct, OilProductResponse } from './product.model';

@Injectable({ providedIn: 'root' })
export class OilProductsService {
  private oilProducts: OilProduct[] = [];
  private updatedOilProducts = new Subject<OilProduct[]>();

  private API_URL: String = 'http://localhost:1044/api/v1/';
  constructor(private http: HttpClient, private router: Router) {}

  getOilProducts() {
    return this.http
      .get<{}>(this.API_URL + 'grades')
      .subscribe((productData) => {
        console.log(productData);
        this.oilProducts = productData as OilProduct[];
        this.updatedOilProducts.next([...this.oilProducts]);
      });
  }

  getOilProductListener() {
    return this.updatedOilProducts.asObservable();
  }

  addOilProduct(oilProduct: OilProduct) {
    return this.http
      .post<{ addedOilProduct: OilProductResponse }>(
        this.API_URL + 'grades',
        oilProduct
      )
      .subscribe((productData) => {
        console.log(productData.addedOilProduct.message);
        this.oilProducts.push(oilProduct);
        this.updatedOilProducts.next([...this.oilProducts]);
        this.router.navigate(['/settings']);
      });
  }

  updateOilProduct(oilProduct: OilProduct) {
    return this.http
      .put<{ updated: OilProductResponse }>(
        this.API_URL + 'grades' + oilProduct.id,
        oilProduct
      )
      .subscribe((productData) => {
        console.log(productData.updated.message);
        this.router.navigate(['/settings']);
      });
  }

  deleteOilProduct(oilProductId: String) {
    return this.http
      .delete<{ deletedOilProduct: OilProductResponse }>(
        this.API_URL + 'grades' + oilProductId
      )
      .subscribe((productData) => {
        console.log(productData.deletedOilProduct.message);
        const updatedOilProduct = this.oilProducts.filter(
          (product) => product.id != oilProductId
        );
        this.oilProducts = updatedOilProduct;
        this.updatedOilProducts.next([...this.oilProducts]);
      });
  }
}
