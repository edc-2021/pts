import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { OilProduct } from './product.model';
import { OilProductsService } from './product.service';

@Component({
  selector: 'app-settings-component',
  templateUrl: './settings.component.html',
})
export class SettingsComponent implements OnInit, OnDestroy {
  oilProducts: OilProduct[] = [];
  private oilProductSubscription: Subscription = new Subscription();
  constructor(private oilProductService: OilProductsService) {}
  ngOnInit() {
    this.oilProductService.getOilProducts();
    this.oilProductSubscription = this.oilProductService
      .getOilProductListener()
      .subscribe((oilProducts: OilProduct[]) => {
        this.oilProducts = oilProducts;
      });
  }

  ngOnDestroy() {
    this.oilProductSubscription.unsubscribe();
  }
}
