export interface OilProduct {
  id: string;
  description: string;
  unitPrice: string;
  taxId: string;
}

export interface OilProductResponse {
  code: string;
  message: string;
}


