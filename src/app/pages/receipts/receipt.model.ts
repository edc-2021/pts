export interface Receipt {
  id: 'string';
  amountPaid: 0;
  paymentType: 'string';
  salesDate: 'string';
  discount: 0;
  amountTaxExcl: 0;
  amountTaxIncl: 0;
  customerName: 'string';
  customerVrn: 'string';
  cardNum: 'string';
  cardType: 'string';
  tin: 'string';
  vrn: 'string';
  taxPayerName: 'string';
  regId: 'string';
  serialNumber: 'string';
  deviceType: 'string';
  uin: 'string';
  date: 'string';
  time: 'string';
  dc: 0;
  gc: 0;
  znum: 'string';
  rctVnum: 'string';
  rctNumber: 'string';
}
