import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Receipt } from './receipt.model';
import { ReceiptService } from './receipt.service';

declare var $: any;
@Component({
  selector: 'app-receipts-component',
  templateUrl: './receipts.component.html',
})
export class ReceiptComponent implements OnInit, OnDestroy {
  receipts: Receipt[] = [];
  private receiptSubscription: Subscription = new Subscription();
  constructor(private receiptService: ReceiptService) {}
  ngOnInit() {
    this.loadJsScripts();
    this.receiptService.getReceipts();
    this.receiptSubscription = this.receiptService
      .getReceiptsUpdateListener()
      .subscribe((receipts: Receipt[]) => {
        this.receipts = receipts;
      });
  }

  ngOnDestroy() {
    this.receiptSubscription.unsubscribe();
  }

  private loadJsScripts() {
    $('#datatables-basic').DataTable({
      responsive: true,
    });

    // Datatables with Buttons
    var datatablesButtons = $('#datatables-buttons').DataTable({
      lengthChange: !1,
      buttons: ['print'],
      responsive: true,
    });
    datatablesButtons
      .buttons()
      .container()
      .appendTo('#datatables-buttons_wrapper .col-md-6:eq(0)');
  }
}
