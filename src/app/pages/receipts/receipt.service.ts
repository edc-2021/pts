import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { Receipt } from './receipt.model';

@Injectable({ providedIn: 'root' })
export class ReceiptService {
  private receipts: Receipt[] = [];
  private updatedReceipts = new Subject<Receipt[]>();

  private API_URL = 'http://localhost:1044/api/v1/';
  constructor(private http: HttpClient, private router: Router) {}

  getReceipts() {
    return this.http
      .get<{}>(this.API_URL + 'receipts')
      .subscribe((receiptData) => {
        console.log(receiptData);
        this.receipts = receiptData as Receipt[];
        this.updatedReceipts.next([...this.receipts]);
      });
  }

  getReceiptsUpdateListener() {
    return this.updatedReceipts.asObservable();
  }
}
