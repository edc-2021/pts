import { Component } from '@angular/core';
import { AuthService } from '../../auth/auth.service';

@Component({
  templateUrl: './header.component.html',
  selector: 'app-header-component',
})
export class HeaderComponent {
  constructor(private authService: AuthService) {}

  onLogOut() {
    this.authService.logout();
  }
}
