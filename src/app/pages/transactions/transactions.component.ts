import { Component } from '@angular/core';

declare var $: any;
@Component({
  selector: 'app-transactions-component',
  templateUrl: './transactions.component.html',
})
export class TransactionsComponent {
  ngOnInit() {
    this.loadScripts();
  }

  private loadScripts() {
    // Datatables basic
    $('#datatables-basic').DataTable({
      responsive: true,
    });
    // Datatables with Buttons
    var datatablesButtons = $('#datatables-buttons').DataTable({
      lengthChange: !1,
      buttons: ['print'],
      responsive: true,
    });
    datatablesButtons
      .buttons()
      .container()
      .appendTo('#datatables-buttons_wrapper .col-md-6:eq(0)');
  }
}
