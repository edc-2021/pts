import { Component, OnInit } from '@angular/core';

import { DynamicScriptLoaderService } from '../DynamicScriptLoaderService';

declare var $: any;
@Component({
  selector: 'app-component-dashboard',
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit {
  constructor(private dynamicScriptLoaderService: DynamicScriptLoaderService) {}

  ngOnInit() {
    this.loadScripts();
  }

  private loadScripts() {
    // You can load multiple scripts by just providing the key as argument into load method of the service
    this.dynamicScriptLoaderService
      .load('dashboard')
      .then((data) => {
        // Script Loaded Successfully
      })
      .catch((error) => console.log(error));
  }
}
