import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoginData } from '../auth.model';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-component-login',
  templateUrl: './login.component.html',
})
export class LoginComponent {
  constructor(private authService: AuthService) {}
  onLogin(form: NgForm) {
    if (form.invalid) return;
    const loginData: LoginData = {
      username: form.value.email,
      password: form.value.password,
    };
    console.log(loginData);
    this.authService.login(loginData);
  }
}
