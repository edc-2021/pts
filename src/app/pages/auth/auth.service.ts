import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { LoginData } from './auth.model';

@Injectable({ providedIn: 'root' })
export class AuthService {
  private token: String;
  private API_URL = 'http://localhost:1044/api/v1/users/';

  private authStatusListener = new Subject<boolean>();
  private isAuthenticated = false;

  //inject http client
  constructor(private http: HttpClient, private router: Router) {}

  getToken() {
    return this.token;
  }
 
  getAuthStatusListener() {
    return this.authStatusListener.asObservable();
  }

  getAuthenticatedStatus() {
    return this.isAuthenticated;
  }

  /*
  createUser(userData: UserData) {
    this.http
      .post<{
        id: string;
        email: string;
        firstname: string;
        lastname: string;
        createdAt: string;
      }>('http://localhost:3000/api/v1/auth/signup', userData)
      .subscribe((res) => {
        console.log(res);
        if (res.id) {
          this.router.navigate(['/login']);
        }
      });
  } */

  login(loginData: LoginData) {
    this.http
      .post<{ accessToken: string }>(this.API_URL + 'auth', loginData)
      .subscribe(
        (res) => {
          // console.log(res);
          this.token = res.accessToken;
          if (res.accessToken) {
            this.authStatusListener.next(true);
            this.isAuthenticated = true;
            this.saveAuthData(res.accessToken);
            this.router.navigate(['/']);
          }
        },
        (error) => {
          console.log(error.error.Error);
          console.log('Login error');
        }
      );
  }

  logout() {
    this.token = null;
    this.isAuthenticated = false;
    this.authStatusListener.next(false);
    this.clearAuthData();
    this.router.navigate(['/login']);
  }

  saveAuthData(token: string) {
    localStorage.setItem('token', token);
  }

  //For auto login
  autoAuthUser() {
    const authInformation = this.getAuthData();
    if (!authInformation) {
      return;
    }
    this.authStatusListener.next(true);
    this.token = authInformation.token;
    this.isAuthenticated = true;
  }

  clearAuthData() {
    localStorage.removeItem('token');
  }

  getAuthData() {
    const token = localStorage.getItem('token');
    if (!token) {
      return null;
    }
    return {
      token,
    };
  }
}
