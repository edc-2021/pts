import {
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';

//Inject a service inside a service
@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  //just for outgoing
  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const authToken = this.authService.getToken();
    console.log('token', authToken);
    const authRequest = req.clone({
      headers: req.headers.set('apiKey', 'Bearer ' + authToken),
    });
    return next.handle(authRequest);
  }
}
