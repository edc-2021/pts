export interface LoginData {
  username: string;
  password: string;
}

export interface LoginDataSuccess {
  accessToken: string;
}

export interface LoginDataFailure {
  code: string;
  message: string;
}

export interface UserData {
  name: string;
  phone: string;
  email: string;
  username: string;
}

export interface UserForgotPassword {
  email: string;
}

export interface userChangePassword {
  password: string;

}
