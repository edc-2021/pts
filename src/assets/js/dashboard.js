setTimeout(function () {
  $(function () {
    // Line chart
    new Chart(document.getElementById("chartjs-dashboard-line"), {
      type: "line",
      data: {
        labels: ["01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00",
        "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00",
        "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00",
        "22:00", "23:00"],
        datasets: [
          {
            label: "Petrol",
            fill: true,
            backgroundColor: window.theme.primary,
            borderColor: window.theme.primary,
            borderWidth: 1,
            data: [390, 290, 300, 478, 935, 1012, 211,
              390, 290, 300, 478, 935, 1012, 211,
              390, 290, 300, 478, 935, 1012, 211],
          },
          {
            label: "Diesel",
            fill: true,
            backgroundColor: window.theme.warning,
            borderColor: window.theme.warning,
            borderWidth: 1,
            data: [1335, 2345, 334, 4133, 5356, 534, 853,
              1335, 2345, 334, 4133, 5356, 534, 853,
              1335, 2345, 334, 4133, 5356, 534, 853],
          },
          {
            label: "Kerosene",
            fill: true,
            backgroundColor: window.theme.danger,
            borderColor: window.theme.danger,
            borderWidth: 1,
            data: [1313, 2135, 334, 423, 5335, 551, 650, 450,
              1313, 2135, 334, 423, 5335, 551, 650, 450,
              1313, 2135, 334, 423, 5335, 551, 650, 450],
          },
        ],
      },
      options: {
        maintainAspectRatio: false,
        legend: {
          display: true,
        },
        tooltips: {
          intersect: false,
        },
        hover: {
          intersect: true,
        },
        plugins: {
          filler: {
            propagate: false,
          },
        },
        elements: {
          point: {
            radius: 0,
          },
        },
        scales: {
          xAxes: [
            {
              reverse: true,
              gridLines: {
                color: "rgba(0,0,0,0.0)",
              },
            },
          ],
          yAxes: [
            {
              ticks: {
                stepSize: 5,
              },
              display: true,
              gridLines: {
                color: "rgba(0,0,0,0)",
                fontColor: "#fff",
              },
            },
          ],
        },
      },
    });
  });
}, 1000);


setTimeout(function () {
  $(function () {
    // Line chart
    new Chart(document.getElementById("graph-bar-chart"), {
      type: "bar",
      data: {
        labels: ["Mon", "Tue", "Wed", "Thr", "Fri", "Sat", "Sun"],
        datasets: [
          {
            label: "Petrol",
            fill: true,
            backgroundColor: window.theme.primary,
            borderColor: window.theme.primary,
            borderWidth: 1,
            data: [1, 0, 3.8, 0, 5.0, 0, 0],
          },
          {
            label: "Diesel",
            fill: true,
            backgroundColor: window.theme.warning,
            borderColor: window.theme.warning,
            borderWidth: 1,
            data: [0.5, 0, 0, 3, 0, 0, 0],
          },
         
        ],
      },
      options: {
        maintainAspectRatio: false,
        legend: {
          display: true,
        },
        tooltips: {
          intersect: false,
        },
        hover: {
          intersect: true,
        },
        plugins: {
          filler: {
            propagate: false,
          },
        },
        elements: {
          point: {
            radius: 0,
          },
        },
        scales: {
          xAxes: [
            {
              reverse: true,
              gridLines: {
                color: "rgba(0,0,0,0.0)",
              },
            },
          ],
          yAxes: [
            {
              ticks: {
                stepSize: 5,
              },
              display: true,
              gridLines: {
                color: "rgba(0,0,0,0)",
                fontColor: "#fff",
              },
            },
          ],
        },
      },
    });
  });
}, 1000);

setTimeout(function () {
  $(function () {
    // Line chart
    new Chart(document.getElementById("graph-line-chart"), {
      type: "line",
      data: {
        labels: ["Mon", "Tue", "Wed", "Thr", "Fri", "Sat", "Sun"],
        datasets: [
          {
            label: "Sales",
            fill: true,
            backgroundColor: "#ffff",
            borderColor: window.theme.primary,
            borderWidth: 1,
            data: [0, 0, 7000, 0, 0, 0, 0],
          },
        ],
      },
      options: {
        maintainAspectRatio: false,
        legend: {
          display: true,
        },
        tooltips: {
          intersect: false,
        },
        hover: {
          intersect: true,
        },
        plugins: {
          filler: {
            propagate: false,
          },
        },
        elements: {
          point: {
            radius: 0,
          },
        },
        scales: {
          xAxes: [
            {
              reverse: true,
              gridLines: {
                color: "rgba(0,0,0,0.0)",
              },
            },
          ],
          yAxes: [
            {
              ticks: {
                stepSize: 5,
              },
              display: true,
              gridLines: {
                color: "rgba(0,0,0,0)",
                fontColor: "#fff",
              },
            },
          ],
        },
      },
    });
  });
}, 1000);

setTimeout(function () {
  $(function () {
    // Line chart
    new Chart(document.getElementById("chartjs-dashboard-yearly"), {
      type: "bar",
      data: {
        labels: [
          "Jan",
          "Feb",
          "Mar",
          "Apr",
          "May",
          "Jun",
          "Jul",
          "Aug",
          "Sep",
          "Oct",
          "Nov",
          "Dec",
        ],
        datasets: [
          {
            label: "Petrol",
            fill: true,
            backgroundColor: window.theme.primary,
            borderColor: window.theme.primary,
            borderWidth: 1,
            data: [
              190,
              290,
              300,
              478,
              590,
              580,
              635,
              836,
              935,
              1012,
              111,
              1234,
            ],
          },
          {
            label: "Diesel",
            fill: true,
            backgroundColor: window.theme.warning,
            borderColor: window.theme.warning,
            borderWidth: 1,
            data: [
              1335,
              2345,
              334,
              4133,
              5356,
              534,
              623,
              8532,
              9345,
              1013,
              1113,
              1532,
            ],
          },
          {
            label: "Kerosene",
            fill: true,
            backgroundColor: window.theme.danger,
            borderColor: window.theme.danger,
            borderWidth: 1,
            data: [
              1313,
              2135,
              334,
              423,
              5335,
              551,
              65,
              834,
              923,
              150,
              1134,
              1213,
            ],
          },
        ],
      },
      options: {
        maintainAspectRatio: false,
        legend: {
          display: false,
        },
        tooltips: {
          intersect: false,
        },
        hover: {
          intersect: true,
        },
        plugins: {
          filler: {
            propagate: false,
          },
        },
        elements: {
          point: {
            radius: 0,
          },
        },
        scales: {
          xAxes: [
            {
              reverse: true,
              gridLines: {
                color: "rgba(0,0,0,0.0)",
              },
            },
          ],
          yAxes: [
            {
              ticks: {
                stepSize: 5,
              },
              display: true,
              gridLines: {
                color: "rgba(0,0,0,0)",
                fontColor: "#fff",
              },
            },
          ],
        },
      },
    });
  });
}, 1000);

setTimeout(function () {
  $(function () {
    // Pie chart
    new Chart(document.getElementById("chartjs-dashboard-pie"), {
      type: "pie",
      data: {
        labels: ["Unleaded", "Diesel", "Kerosene"],
        datasets: [
          {
            data: [7000, 200, 0],
            backgroundColor: [
              window.theme.warning,
              window.theme.danger,
              "#E8EAED",
            ],
            borderColor: "transparent",
          },
        ],
      },
      options: {
        responsive: !window.MSInputMethodContext,
        maintainAspectRatio: false,
        legend: {
          display: false,
        },
        cutoutPercentage: 75,
      },
    });
  });
}, 1000);

setTimeout(function () {
  $(function () {
    $("#datatables-dashboard-projects").DataTable({
      pageLength: 6,
      lengthChange: false,
      bFilter: false,
      autoWidth: false,
    });
  });
}, 1000);

setTimeout(function () {
  $(function () {
    $("#datetimepicker-dashboard").datetimepicker({
      inline: true,
      sideBySide: false,
      format: "L",
    });
  });
}, 1000);
